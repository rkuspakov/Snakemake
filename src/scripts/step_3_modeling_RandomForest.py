# Random Forest

import os
import pickle

import hydra
import pandas as pd
from omegaconf import DictConfig
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


@hydra.main(version_base=None, config_path="../conf", config_name="config.yaml")
def main(cfg: DictConfig) -> None:
    preprocessing_numbers = cfg.preprocessing_numbers

    for preprocessing_number in range(1, preprocessing_numbers + 1):
        df = pd.read_csv(f"data/processed/processed_data_{preprocessing_number}.csv")

        columns_to_keep = [
            "tree_dbh",
            "curb_loc",
            "steward",
            "guards",
            "sidewalk",
            "root_stone",
            "root_grate",
            "root_other",
            "trunk_wire",
            "trnk_light",
            "trnk_other",
            "brch_light",
            "brch_shoe",
            "brch_other",
            "postcode",
            "community board",
            "borocode",
            "cncldist",
            "st_assem",
            "st_senate",
            "boro_ct",
            "latitude",
            "longitude",
            "x_sp",
            "y_sp",
            "name_American beech",
            "name_American elm",
            "name_American hophornbeam",
            "name_American hornbeam",
            "name_American larch",
            "name_American linden",
            "name_Amur cork tree",
            "name_Amur maackia",
            "name_Amur maple",
            "name_Atlantic white cedar",
            "name_Atlas cedar",
            "name_Callery pear",
            "name_Chinese chestnut",
            "name_Chinese elm",
            "name_Chinese fringetree",
            "name_Chinese tree lilac",
            "name_Cornelian cherry",
            "name_Douglas-fir",
            "name_English oak",
            "name_European alder",
            "name_European beech",
            "name_European hornbeam",
            "name_Himalayan cedar",
            "name_Japanese hornbeam",
            "name_Japanese maple",
            "name_Japanese snowbell",
            "name_Japanese tree lilac",
            "name_Japanese zelkova",
            "name_Kentucky coffeetree",
            "name_Kentucky yellowwood",
            "name_London planetree",
            "name_Norway maple",
            "name_Norway spruce",
            "name_Ohio buckeye",
            "name_Oklahoma redbud",
            "name_Osage-orange",
            "name_Persian ironwood",
            "name_Schumard's oak",
            "name_Scots pine",
            "name_Shantung maple",
            "name_Siberian elm",
            "name_Sophora",
            "name_Turkish hazelnut",
            "name_Virginia pine",
            "name_arborvitae",
            "name_ash",
            "name_bald cypress",
            "name_bigtooth aspen",
            "name_black cherry",
            "name_black locust",
            "name_black maple",
            "name_black oak",
            "name_black pine",
            "name_black walnut",
            "name_blackgum",
            "name_blue spruce",
            "name_boxelder",
            "name_bur oak",
            "name_catalpa",
            "name_cherry",
            "name_cockspur hawthorn",
            "name_common hackberry",
            "name_crab apple",
            "name_crepe myrtle",
            "name_crimson king maple",
            "name_cucumber magnolia",
            "name_dawn redwood",
            "name_eastern cottonwood",
            "name_eastern hemlock",
            "name_eastern redbud",
            "name_eastern redcedar",
            "name_empress tree",
            "name_false cypress",
            "name_flowering dogwood",
            "name_ginkgo",
            "name_golden raintree",
            "name_green ash",
            "name_hardy rubber tree",
            "name_hawthorn",
            "name_hedge maple",
            "name_holly",
            "name_honeylocust",
            "name_horse chestnut",
            "name_katsura tree",
            "name_kousa dogwood",
            "name_littleleaf linden",
            "name_magnolia",
            "name_maple",
            "name_mimosa",
            "name_mulberry",
            "name_northern red oak",
            "name_pagoda dogwood",
            "name_paper birch",
            "name_paperbark maple",
            "name_pignut hickory",
            "name_pin oak",
            "name_pine",
            "name_pitch pine",
            "name_pond cypress",
            "name_purple-leaf plum",
            "name_quaking aspen",
            "name_red horse chestnut",
            "name_red maple",
            "name_red pine",
            "name_river birch",
            "name_sassafras",
            "name_sawtooth oak",
            "name_scarlet oak",
            "name_serviceberry",
            "name_shingle oak",
            "name_silver birch",
            "name_silver linden",
            "name_silver maple",
            "name_smoketree",
            "name_southern magnolia",
            "name_southern red oak",
            "name_spruce",
            "name_sugar maple",
            "name_swamp white oak",
            "name_sweetgum",
            "name_sycamore maple",
            "name_tartar maple",
            "name_tree of heaven",
            "name_trident maple",
            "name_tulip-poplar",
            "name_two-winged silverbell",
            "name_weeping willow",
            "name_white ash",
            "name_white oak",
            "name_white pine",
            "name_willow oak",
        ]

        X = df[columns_to_keep]
        y = df["health_status"]

        test_size = cfg["modeling"]["params"]["test_size"]
        random_state = cfg["modeling"]["params"]["random_state"]

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, stratify=y, test_size=test_size, random_state=random_state
        )

        print(
            f"Baseline Accuracy (Processing {preprocessing_number}):",
            y_train.value_counts(normalize=True),
        )

        # Random Forest

        model = RandomForestClassifier(n_estimators=100, max_depth=6, random_state=42)
        model.fit(X_train, y_train)

        print(
            f"Random Forest Scores (Processing {preprocessing_number}):",
            model.score(X_train, y_train),
            model.score(X_test, y_test),
        )

        # Define the directory path for saving files
        models_dir = "models"

        # Ensure that the directory exists, create it if necessary
        if not os.path.exists(models_dir):
            os.makedirs(models_dir)

        with open(
            os.path.join(
                models_dir, f"model_RandomForest_{preprocessing_number}.pickle"
            ),
            "wb",
        ) as f:
            pickle.dump(model, f)


if __name__ == "__main__":
    main()
