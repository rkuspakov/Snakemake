# Snakemake

# Подготовка виртуального окружения

## Установка виртуального окружения conda
Устанавливаем Анаконда

## Создадим виртуальное окружение при помощи conda
conda create -n mlops-sm или conda create --prefix mlops-sm

## Активировать виртуальное окружение
conda activate C:\Project\Snakemake\mlops-sm

## Деактивировать виртуальное окружение (опционально)
conda deactivate

## Snakemake

![dag.svg](dag.svg)

## План работы

1. Установить все конфигурационные параметры в файле src/conf/config.yaml
2. Загрузить  датасет с Kaggle https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data?resource=download 
3. Делаем две предобработки. Один на основном наборе данных, второй получаеться из деления основного набора данных, разделенных на две одинаковые части
4. Обучаем модель по трем алгоритмам: LogisticRegression, DecisionTree, RandomForest, на двух датасетах с использованием  Snakemake
5. На выходе получаем 6 обученных моделей:
    - model_DecisionTree_1.pickle
    - model_DecisionTree_2.pickle
    - model_LogisticRegression_1.pickle
    - model_LogisticRegression_2.pickle
    - model_RandomForest_1.pickle
    - model_RandomForest_2.pickle

## Шаги выполения

Пакетный менеджер использует poetry. Все зависимости зафиксированны в poetry.lock

1. **Установка Poetry**
conda install poetry или pip install poetry

2. **Переходим в папку проекта**: В командной строке переходим в папку проекта


3. **Устанавливаем все зависимости**: poetry install
    (Зависимости устанавливаются из файла pyproject.toml)

4. **Активируем виртуальное окружение (Опционально)**: Poetry создает виртуальное окружение для каждого проекта: poetry shell

5. **Запускаем Snakemake**: Запускаем следующую команду для запуска Snakemake и запуска workflow определенного в файле Snakefile: snakemake --cores 2
